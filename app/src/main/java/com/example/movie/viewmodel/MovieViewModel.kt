package com.example.movie.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movie.model.Movie

class MovieViewModel : ViewModel() {
    var MovieList: MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    var MovieListDB : MutableLiveData<ArrayList<Movie>> = MutableLiveData()
    var MovieFavouriteNumber: MutableLiveData<Int> = MutableLiveData()

    init {
        MovieList.value = ArrayList()
        MovieListDB.value = ArrayList()
        MovieFavouriteNumber.value = 0
    }
}
