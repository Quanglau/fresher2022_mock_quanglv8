package com.example.movie.model

import com.google.gson.annotations.SerializedName

data class CastAndCrew(
    @SerializedName("name") var name: String,
    @SerializedName("profile_path") var profilePath: String,
)
