package com.example.movie.model

import com.google.gson.annotations.SerializedName

data class CastCrewList(
    @SerializedName("id") var id: Int,
    @SerializedName("cast") var castList: List<CastAndCrew>,
    @SerializedName("crew") var crewList: List<CastAndCrew>
)
