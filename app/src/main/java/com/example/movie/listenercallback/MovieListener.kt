package com.example.movie.listenercallback

import com.example.movie.model.Movie

interface MovieListener {
    fun onUpdateFromMovie(movie: Movie, isFavourite: Boolean)
    fun onUpdateTitleMovie(movieTitle: String)
}
