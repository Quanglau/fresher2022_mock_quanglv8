package com.example.movie.listenercallback

import com.example.movie.model.Movie


interface FavouriteListener {
    fun onUpdateFromFavourite(movie: Movie)
}
