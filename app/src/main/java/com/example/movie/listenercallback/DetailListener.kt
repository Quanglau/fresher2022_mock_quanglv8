package com.example.movie.listenercallback

import com.example.movie.model.Movie


interface DetailListener {
    fun onUpdateFromDetail(movie: Movie, isFavourite: Boolean)
    fun onAddReminder()
}
