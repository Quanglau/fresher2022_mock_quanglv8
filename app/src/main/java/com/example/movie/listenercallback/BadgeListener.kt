package com.example.movie.listenercallback

interface BadgeListener {
    fun onUpdateBadgeNumber(isFavourite: Boolean)
}
