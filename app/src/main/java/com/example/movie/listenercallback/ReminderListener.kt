package com.example.movie.listenercallback

import com.example.movie.model.Movie

interface ReminderListener {
    fun onShowDetailReminder(movie: Movie)
}
