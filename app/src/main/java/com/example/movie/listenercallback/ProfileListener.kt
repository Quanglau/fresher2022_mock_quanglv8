package com.example.movie.listenercallback

import android.graphics.Bitmap
import android.provider.ContactsContract

interface ProfileListener {
    fun onSaveProfile(name: String, email: String, dateOfBirth: String, gender: String, bitmapImage: Bitmap)
}
