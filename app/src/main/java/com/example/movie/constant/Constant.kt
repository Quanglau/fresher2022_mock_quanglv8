package com.example.movie.constant

class Constant {
    companion object{
        const val BUNDLE_TITLE_KEY = "movie_title"
        const val BUNDLE_RELEASE_KEY = "movie_release"
        const val BUNDLE_RATING_KEY = "movie_rate"
        const val BUNDLE_ID_KEY = "movie_id"
        const val REMINDER_ALL = 0
        const val REMINDER_PROFILE = 1
        const val PROFILE_AVATAR_KEY = "profile_avatar"
        const val PROFILE_NAME_KEY = "profile_name"
        const val PROFILE_EMAIL_KEY = "profile_email"
        const val PROFILE_BIRTHDAY_KEY = "profile_date_of_birth"
        const val PROFILE_GENDER_KEY = "profile_gender"

    }
}
