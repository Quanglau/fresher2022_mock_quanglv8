package com.example.movie.constant

class APIConstant {
    companion object{
        var BASE_URL = "https://api.themoviedb.org/"
        var API_KEY = "e7631ffcb8e766993e5ec0c1f4245f93"
        var BASE_IMG_URL = "https://image.tmdb.org/t/p/original/"
    }
}
