package com.example.movie.view

import android.app.*
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.os.Bundle
import android.telecom.Call
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movie.R
import com.example.movie.adapters.CastAndCrewAdapter
import com.example.movie.api.ApiInterface
import com.example.movie.api.RetrofitClient
import com.example.movie.broadcastreceiver.AlarmReceiver
import com.example.movie.constant.APIConstant
import com.example.movie.constant.Constant
import com.example.movie.database.DatabaseOpenHelper
import com.example.movie.listenercallback.BadgeListener
import com.example.movie.listenercallback.DetailListener
import com.example.movie.model.CastAndCrew
import com.example.movie.model.CastCrewList
import com.example.movie.model.Movie
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
const val notificationID = 1
const val channelID = "channel1"

class DetailFragment : Fragment(), View.OnClickListener{
    private lateinit var mMovie: Movie
    private lateinit var mFavouritebt: ImageButton
    private lateinit var mDateText: TextView
    private lateinit var mRateText: TextView
    private lateinit var mPosterImg: ImageView
    private lateinit var mRemiderBtn: Button
    private lateinit var mOverviewText: TextView
    private lateinit var mCastRecyclerView: RecyclerView
    private lateinit var mCastAndCrewAdapter: CastAndCrewAdapter
    private lateinit var mCastAndCrewList: ArrayList<CastAndCrew>
    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mDetailListener: DetailListener
    private lateinit var mRemiderTextView: TextView
    private var mSaveYear = 0
    private var mSaveMonth = 0
    private var mSaveDay = 0
    private var mSaveHour = 0
    private var mSaveMinute = 0


    fun setDetailListener(detailListener: DetailListener) {
        this.mDetailListener = detailListener
    }

    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
    }

    fun setDatabaseOpenHelper(databaseOpenHelper: DatabaseOpenHelper) {
        this.mDatabaseOpenHelper = databaseOpenHelper
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        val bundle = this.arguments
        if (bundle != null) mMovie = bundle.getSerializable("movieDetail") as Movie
        mFavouritebt = view.findViewById(R.id.imgStar)
        mDateText = view.findViewById(R.id.release_date)
        mRateText = view.findViewById(R.id.rating)
        mPosterImg = view.findViewById(R.id.img_poster)
        mOverviewText = view.findViewById(R.id.overview)
        mRemiderBtn = view.findViewById(R.id.reminder_bt)
        mRemiderTextView = view.findViewById(R.id.reminder_tv)
        mRemiderBtn.setOnClickListener {
            createReminder()
        }
        mCastRecyclerView = view.findViewById(R.id.listCast)
        if (mMovie.isFavourite) mFavouritebt.setImageResource(R.drawable.ic_starfish1)
        else mFavouritebt.setImageResource(R.drawable.ic_starfish)
        mFavouritebt.setOnClickListener(this)
        mFavouritebt.setOnClickListener(this)
        mDateText.text = mMovie.releaseDate
        mRateText.text = "${mMovie.voteAverage}/10"
        val url = APIConstant.BASE_IMG_URL + mMovie.posterPath
        Picasso.get().load(url).into(mPosterImg)
        mOverviewText.text = mMovie.overview
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.HORIZONTAL
        mCastAndCrewList = ArrayList()
        mCastAndCrewAdapter = CastAndCrewAdapter(mCastAndCrewList)
        mCastRecyclerView.adapter = mCastAndCrewAdapter
        mCastRecyclerView.layoutManager = layoutManager
        getCastAndCrewFromApi()

        return view
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.imgStar -> {
                if (mMovie.isFavourite) {
                    if (mDatabaseOpenHelper.deleteMovie(mMovie.id) > -1) {
                        mFavouritebt.setImageResource(R.drawable.ic_starfish)
                        mMovie.isFavourite = false
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mDetailListener.onUpdateFromDetail(mMovie, false)

                    }
                } else {
                    if (mDatabaseOpenHelper.addMovie(mMovie) > -1)
                        mFavouritebt.setImageResource(R.drawable.ic_starfish1)
                    mMovie.isFavourite = true
                    mBadgeListener.onUpdateBadgeNumber(true)
                    mDetailListener.onUpdateFromDetail(mMovie, true)
                }
            }
        }
    }
    private fun getCastAndCrewFromApi() {
        val retrofit: ApiInterface =
            RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getCastAndCrew(mMovie.id, APIConstant.API_KEY)
        retrofitData.enqueue(object : Callback<CastCrewList> {
            override fun onResponse(call: Call<CastCrewList>?, response: Response<CastCrewList>?) {
                val responseBody = response?.body()
                mCastAndCrewList.addAll(responseBody!!.castList)
                mCastAndCrewList.addAll(responseBody.crewList)
                mCastAndCrewAdapter.updateList(mCastAndCrewList)
            }

            override fun onFailure(call: Call<CastCrewList>?, t: Throwable?) {
                TODO("Not yet implemented")
            }

        })
    }
    fun updateMovie(movieId: Int) {
        if (mMovie.id == movieId) {
            mMovie.isFavourite = false
            mFavouritebt.setImageResource(R.drawable.ic_starfish)
        }
    }
    private fun createReminder() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        val startHour = currentDateTime.get(Calendar.HOUR_OF_DAY)
        val startMinute = currentDateTime.get(Calendar.MINUTE)

        DatePickerDialog(requireContext(), {_, year, month, day ->
            TimePickerDialog(requireContext(), {_, hour, minute ->
                val pickedDateTime = Calendar.getInstance()
                pickedDateTime.set(year, month, day, hour, minute)
                mSaveYear = year
                mSaveMonth = month
                mSaveDay = day
                mSaveHour = hour
                mSaveMinute = minute
                currentDateTime.set(mSaveYear, mSaveMonth, mSaveDay, mSaveHour, mSaveMinute)
                val time: Long = currentDateTime.timeInMillis
                val timeHour = "$mSaveYear-$mSaveMonth-$mSaveDay $mSaveHour:$mSaveMinute"
                mRemiderTextView.text = timeHour
                mRemiderTextView.visibility = View.VISIBLE
                mMovie.dateHourTime = timeHour
                mMovie.reminderTime = time.toString()
                if (mDatabaseOpenHelper.checkReminderExist(mMovie.id) > 0) {
                    mDatabaseOpenHelper.updateReminder(mMovie)
                    createNotification(time)
                }
                else if (mDatabaseOpenHelper.addReminder(mMovie) > 0) {
                    createNotification(time)
                    mDetailListener.onAddReminder()
                }


            }, startHour, startMinute, true).show()
        }, startYear,startMonth, startDay).show()

    }
    private fun createNotification(time: Long) {
        val intent = Intent(context?.applicationContext, AlarmReceiver::class.java)
        val bundle = Bundle()
        bundle.putInt(Constant.BUNDLE_ID_KEY, mMovie.id)
        bundle.putString(Constant.BUNDLE_TITLE_KEY, mMovie.title)
        bundle.putString(Constant.BUNDLE_RELEASE_KEY, mMovie.releaseDate)
        bundle.putDouble(Constant.BUNDLE_RATING_KEY, mMovie.voteAverage)
        bundle.putString("time", mMovie.dateHourTime)
        intent.putExtras(bundle)
        val pendingIntent = PendingIntent.getBroadcast(
            activity?.applicationContext, notificationID, intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )
        val alarmManager = activity?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, time, pendingIntent)
    }


}
