package com.example.movie.view
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.Menu
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.movie.database.DatabaseOpenHelper
import com.example.movie.listenercallback.BadgeListener
import com.example.movie.listenercallback.FavouriteListener
import com.example.movie.model.Movie
import com.example.movie.viewmodel.MovieViewModel
import com.example.movie.R
import com.example.movie.adapters.MovieAdapter

class FavouriteFragment(private var mDatabaseOpenHelper: DatabaseOpenHelper) : Fragment(), View.OnClickListener{
    private val mMovieViewModel: MovieViewModel by activityViewModels()
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mMovieAdapter: MovieAdapter
    private lateinit var mMovieList : ArrayList<Movie>
    private var mCountFavouriteMovie: Int = 0
    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mHomeFavouriteListener: FavouriteListener
    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
    }
    fun setFavouriteListener(mFavouriteListener: FavouriteListener) {
        this.mHomeFavouriteListener = mFavouriteListener
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_favourite, container, false)
        mRecyclerView = view.findViewById(R.id.recycler_view)
        loadFavouriteList()
        mMovieViewModel.MovieListDB.value = mMovieList
        mMovieViewModel.MovieListDB.observe(requireActivity(),{
            mMovieList = it
            mMovieAdapter.updateData(mMovieList)
        })
        setHasOptionsMenu(true)
        return view
    }

    private fun loadFavouriteList() {
        mMovieList = mDatabaseOpenHelper.getListMovie()
        mMovieAdapter = MovieAdapter(mMovieList, 1, true, this)
        mRecyclerView.layoutManager = LinearLayoutManager(activity)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mMovieAdapter
        mCountFavouriteMovie = mMovieList.size
    }
    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.add_fav -> {
                val position = view.tag as Int
                val movieItem = mMovieList[position]
                if (movieItem.isFavourite) {
                    if (mDatabaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        mMovieList.remove(movieItem)
                        mMovieAdapter.notifyItemChanged(position)
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mHomeFavouriteListener.onUpdateFromFavourite(movieItem)
                    }
                }
            }
        }
    }
    fun updateFavouriteList(movie: Movie, isFavourite: Boolean) {
        var position = -1
        for (i in 0 until mMovieList.size) {
            if (mMovieList[i].id == movie.id) {
                position = i
                break
            }
        }
        if (isFavourite) {
            mMovieList.add(movie)
        } else if(position != -1) {
            mMovieList.removeAt(position)
        }
        mMovieAdapter.updateData(mMovieList)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        val item = menu.findItem(R.id.change_layout)
        item.isVisible = false
    }

}
