package com.example.movie.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.movie.R

class RootFragment(private var mHomeFragment: HomeFragment) : Fragment(){
    fun setHomeFragment(homeFragment: HomeFragment) {
        this.mHomeFragment = homeFragment
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_root, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val transaction = requireFragmentManager().beginTransaction()
        transaction.replace(R.id.root, mHomeFragment)
        transaction.commit()
    }
}
