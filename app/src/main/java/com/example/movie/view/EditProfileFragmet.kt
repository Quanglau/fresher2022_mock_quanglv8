package com.example.movie.view

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import com.example.movie.R
import com.example.movie.constant.Constant
import com.example.movie.listenercallback.ProfileListener
import com.example.movie.listenercallback.ToolbarTitleListener
import java.util.*

class EditProfileFragment : Fragment(), View.OnClickListener {
    private lateinit var mCancelbt: Button
    private lateinit var mAvatarimg: ImageView
    private lateinit var mDonebt: Button
    private lateinit var mNameet: EditText
    private lateinit var mEmailet: EditText
    private lateinit var mDateOfBirthet: EditText
    private lateinit var mMalerb: RadioButton
    private lateinit var mFemalerb: RadioButton
    private lateinit var mGenderrg: RadioGroup
    private lateinit var mBitmapProfile: Bitmap
    private lateinit var mProfileListener: ProfileListener
    private lateinit var mSharedPreferences: SharedPreferences
    private var mGender: String = ""
    private val mCameraResultLancher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        if (result.resultCode == Activity.RESULT_OK) {
            val imageBitmap = result.data?.extras?.get("data") as Bitmap
            this.mBitmapProfile = imageBitmap
            mAvatarimg.setImageBitmap(imageBitmap)
        }

    }
    private val mConverterImg: BitmapConverter = BitmapConverter()
    private lateinit var mToolbarTitleListener: ToolbarTitleListener
    fun setProfileListener(profileListener: ProfileListener) {
        this.mProfileListener = profileListener
    }
    fun setToolbarTitleListener(toolbarTitleListener: ToolbarTitleListener) {
        mToolbarTitleListener = toolbarTitleListener
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit_profile, container, false)
        // find view item
        mCancelbt = view.findViewById(R.id.ep_cancel)
        mAvatarimg = view.findViewById(R.id.ep_avatar)
        mDonebt = view.findViewById(R.id.ep_done)
        mNameet = view.findViewById(R.id.ep_name)
        mEmailet = view.findViewById(R.id.ep_email)
        mDateOfBirthet = view.findViewById(R.id.ep_date)
        val bundle = arguments
        if (bundle != null) {
            try {
                mAvatarimg.setImageBitmap(mConverterImg.decodeBase64(bundle.getString(Constant.PROFILE_AVATAR_KEY)))
            } catch (e: Exception){
                mAvatarimg.setImageResource(R.mipmap.avatar)
            }
            val name = bundle.getString(Constant.PROFILE_NAME_KEY)
            val email = bundle.getString(Constant.PROFILE_EMAIL_KEY)
            val gender = bundle.getString(Constant.PROFILE_GENDER_KEY)
            val dateOfBirth = bundle.getString(Constant.PROFILE_BIRTHDAY_KEY)
            mNameet.setText("$name")
            mEmailet.setText("$email")
            mDateOfBirthet.setText("$dateOfBirth")
            if (gender == "Male") {
                mMalerb.isChecked = true
            } else if (gender == "Female") {
                mFemalerb.isChecked = true
            }

        }
        mDateOfBirthet.setOnClickListener{
            pickDateOfBirth()
        }
        mMalerb = view.findViewById(R.id.ep_male)
        mFemalerb = view.findViewById(R.id.ep_female)
        mGenderrg = view.findViewById(R.id.ep_gender)

        //set avatar
        mAvatarimg.setOnClickListener(this)
        //click gender
        onClickChange()
        // save profile
        mDonebt.setOnClickListener(this)
        return view
    }

    private fun onClickChange() {
        mGenderrg.setOnCheckedChangeListener{_, i ->
            if (i == R.id.ep_male) mGender = "Male"
            else if (i == R.id.ep_female) mGender = "Female"
        }
    }
    override fun onClick(view: View?) {
        when(view?.id) {
            R.id.ep_avatar -> pickAvatar()
            R.id.ep_done -> {
                val name = mNameet.text.toString()
                val email = mEmailet.text.toString()
                val date_of_birth = mDateOfBirthet.text.toString()
                mProfileListener.onSaveProfile(name,email,date_of_birth, mGender, mBitmapProfile)
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            }
        }
    }

    private fun pickAvatar() {
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        mCameraResultLancher.launch(cameraIntent)
    }

    private fun pickDateOfBirth() {
        val currentDateTime = Calendar.getInstance()
        val startYear = currentDateTime.get(Calendar.YEAR)
        val startMonth = currentDateTime.get(Calendar.MONTH)
        val startDay = currentDateTime.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(requireContext(), { _, year, month, day ->
            val pickedDateTime = Calendar.getInstance()
            pickedDateTime.set(year, month, day)
            currentDateTime.set(year, month, day)
            val timeHour = "$year/$month/$day"
            mDateOfBirthet.setText(timeHour, TextView.BufferType.EDITABLE)
        }, startYear, startMonth, startDay).show()

    }
}
