package com.example.movie.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.activity.viewModels
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.movie.R
import com.example.movie.adapters.MovieAdapter
import com.example.movie.api.ApiInterface
import com.example.movie.api.RetrofitClient
import com.example.movie.constant.APIConstant
import com.example.movie.database.DatabaseOpenHelper
import com.example.movie.listenercallback.BadgeListener
import com.example.movie.listenercallback.DetailListener
import com.example.movie.listenercallback.FavouriteListener
import com.example.movie.listenercallback.MovieListener
import com.example.movie.model.Movie
import com.example.movie.model.MovieList
import com.example.movie.viewmodel.MovieViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeFragment() : Fragment(), View.OnClickListener{
    private val mMovieViewModel: MovieViewModel by activityViewModels()
    private var mScreenType: Int = 1
    var mMovieDetailFragment = DetailFragment()
    private var mMovieList = ArrayList<Movie>()
    private lateinit var mBadgeListener: BadgeListener
    private lateinit var mMovieRecyclerView: RecyclerView
    private lateinit var mMovieAdapter: MovieAdapter
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mGridLayoutManager: GridLayoutManager
    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private lateinit var mMovieListDB: ArrayList<Movie>
    private lateinit var mMovieListener: MovieListener
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private var mPage = 0
    private lateinit var mDetailListener: DetailListener

    fun setDetailListener(detailListener: DetailListener){
        mMovieDetailFragment.setDetailListener(detailListener)
    }
    fun setBadgeListener(badgeListener: BadgeListener) {
        this.mBadgeListener = badgeListener
        mMovieDetailFragment.setBadgeListener(badgeListener)
    }
    fun setMovieListener(movieListener: MovieListener) {
        this.mMovieListener = movieListener
    }
    private fun setScreenType(screenType: Int) {
        this.mScreenType = screenType
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        mMovieAdapter = MovieAdapter(mMovieList, mScreenType, false, this)
        mMovieRecyclerView = view.findViewById(R.id.recycler_view)
        mProgressBar = view.findViewById(R.id.progress_bar)
        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout)
        mDatabaseOpenHelper = activity?.let { DatabaseOpenHelper(it, "movie_database", null, 1) }!!
        mMovieDetailFragment.setDatabaseOpenHelper(mDatabaseOpenHelper)
        mMovieListDB = mDatabaseOpenHelper.getListMovie()
        updateMovieList()
        getListMovieFromApi(false, false)
        mSwipeRefreshLayout.setOnRefreshListener {
            mSwipeRefreshLayout.isRefreshing = false
            updateMovieList()
            getListMovieFromApi(true, false)
        }
        onLoadMoreListener()
        mMovieViewModel.MovieList.observe(requireActivity()) {
            mMovieList.addAll(it)
            mMovieAdapter.updateData(mMovieList)
        }
        return view
    }


    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.add_fav -> {
                val position = view.tag as Int
                val movieItem = mMovieList[position]
                if (movieItem.isFavourite) {
                    if (mDatabaseOpenHelper.deleteMovie(movieItem.id) > -1) {
                        movieItem.isFavourite = false
                        mBadgeListener.onUpdateBadgeNumber(false)
                        mMovieListener.onUpdateFromMovie(movieItem, false)
                    }
                } else {
                    if (mDatabaseOpenHelper.addMovie(movieItem) > -1)
                        movieItem.isFavourite = true
                    mBadgeListener.onUpdateBadgeNumber(true)
                    mMovieListener.onUpdateFromMovie(movieItem, true)
                }
                mMovieAdapter.notifyItemChanged(position)
            }
            R.id.movie_item -> {
                val position = view.tag as Int
                val movieItem = mMovieList[position]
                val bundle = Bundle()
                bundle.putSerializable("movieDetail", movieItem)
                mMovieDetailFragment.arguments = bundle
                val trans = requireFragmentManager().beginTransaction()
                trans.replace(R.id.root, mMovieDetailFragment)
                trans.addToBackStack(null)
                trans.commit()
                mMovieListener.onUpdateTitleMovie(movieItem.title)

            }
        }
    }

    companion object {

        @JvmStatic
        fun newInstance(mScreenType: Int) = HomeFragment().apply {
            arguments = Bundle().apply {
                putInt("aaa", mScreenType)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        arguments?.getInt("aaa")?.let { mScreenType = it }
    }

    fun changeViewHome() {
        if (mMovieRecyclerView.layoutManager == mGridLayoutManager) {
            mMovieRecyclerView.layoutManager = mLinearLayoutManager
            mMovieAdapter = MovieAdapter(mMovieList, 1, false,this)
            setScreenType(1)
        } else {
            mMovieRecyclerView.layoutManager = mGridLayoutManager
            mMovieAdapter = MovieAdapter(mMovieList, 0, false,this)
            setScreenType(0)
        }
        mMovieRecyclerView.adapter = mMovieAdapter
        mMovieAdapter.notifyDataSetChanged()
    }
    fun updateMovieList(movie: Movie, isFavourite: Boolean) {
        var position = -1
        for (i in 0 until mMovieList.size) {
            if (mMovieList[i].id == movie.id) {
                position = i
                break
            }
        }
        if (position != -1) {
            mMovieList[position].isFavourite = isFavourite
            mMovieAdapter.notifyDataSetChanged()
        }
    }
    private fun updateMovieList() {
        mPage = 1
        mMovieList = ArrayList()
        mLinearLayoutManager = LinearLayoutManager(activity)
        mGridLayoutManager = GridLayoutManager(activity, 2)
        if (mScreenType == 1) {
            mMovieRecyclerView.layoutManager = mLinearLayoutManager
        } else mMovieRecyclerView.layoutManager = mGridLayoutManager
        mMovieRecyclerView.adapter = mMovieAdapter
    }
    private fun getListMovieFromApi(isLoadMore: Boolean, isRefresh: Boolean) {
        if (isLoadMore) {
            mPage++
        } else if (!isRefresh) {
            mProgressBar.visibility = View.VISIBLE
        }
        val retrofit : ApiInterface = RetrofitClient().getRetrofitInstance().create(ApiInterface::class.java)
        val retrofitData = retrofit.getMovieList("popular", APIConstant.API_KEY, "$mPage")
        retrofitData.enqueue(object : Callback<MovieList> {
            override fun onResponse(call: Call<MovieList>?, response: Response<MovieList>?) {
                val responseBody = response?.body()
                val listMovieResult = responseBody?.results as ArrayList<Movie>
//                mMovieList.addAll(listMovieResult)
                mMovieViewModel.MovieList.value = listMovieResult
//                mMovieAdapter.updateData(mMovieList)
                mMovieAdapter.settingMovieFavourite(mMovieListDB)
//                mMovieAdapter.notifyDataSetChanged()
                if (!isLoadMore && !isRefresh) {
                    mProgressBar.visibility = View.GONE
                }
                if (isRefresh) {
                    mSwipeRefreshLayout.isRefreshing = false
                }
            }

            override fun onFailure(call: Call<MovieList>?, t: Throwable?) {
                if (isLoadMore) mPage --
                else if (isRefresh)  mSwipeRefreshLayout.isRefreshing = false
                else mProgressBar.visibility = View.GONE
            }

        })
    }
    private fun onLoadMoreListener() {
        mMovieRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if(isLastItemDisplaying(recyclerView)) {
                    getListMovieFromApi(true, false)
                }
            }
        })
    }
    private fun isLastItemDisplaying(recyclerView: RecyclerView) : Boolean {
        if (recyclerView.adapter?.itemCount != 0) {
            val lastVisibleItemPosition =( recyclerView.layoutManager as LinearLayoutManager).findLastCompletelyVisibleItemPosition()
            if (lastVisibleItemPosition!=RecyclerView.NO_POSITION && lastVisibleItemPosition == (recyclerView.adapter?.itemCount?.minus(
                    1
                ))) {
                return true
            }
        }
        return false
    }


}
