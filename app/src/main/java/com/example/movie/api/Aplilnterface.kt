package com.example.movie.api

import com.example.movie.model.CastCrewList
import com.example.movie.model.MovieList

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("3/movie/{typeMovie}")
    fun getMovieList(@Path("typeMovie") typeListMovie : String, @Query("api_key") apiKey: String, @Query("page") pageNumber: String):Call<MovieList>

    @GET("3/movie/{movieId}/credits")
    fun getCastAndCrew(@Path("movieId") id: Int,@Query("api_key") apiKey: String) : Call<CastCrewList>
}
