package com.example.movie.api

import com.example.movie.constant.APIConstant
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitClient {
    lateinit var retrofit: Retrofit
    fun getRetrofitInstance() : Retrofit {
        retrofit = Retrofit.Builder()
            .baseUrl(APIConstant.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit
    }
}
