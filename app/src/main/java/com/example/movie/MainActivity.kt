package com.example.movie

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.viewModels
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.drawerlayout.widget.DrawerLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.movie.adapters.ReminderAdapter
import com.example.movie.adapters.ViewPagerAdapter
import com.example.movie.constant.Constant
import com.example.movie.database.DatabaseOpenHelper
import com.example.movie.databinding.ActivityMainBinding
import com.example.movie.listenercallback.*
import com.example.movie.model.Movie
import com.example.movie.util.BitmapConverter
import com.example.movie.view.*
import com.example.movie.viewmodel.MovieViewModel
import com.google.android.material.tabs.TabLayout


class MainActivity : AppCompatActivity(), BadgeListener, FavouriteListener, MovieListener, DetailListener, ReminderListener, ProfileListener{
    private val mMovieViewModel: MovieViewModel by viewModels()
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding
    private lateinit var mTabLayout: TabLayout
    private lateinit var mViewPager : ViewPager
    private lateinit var mHomeFragment: HomeFragment
    private lateinit var mRootFragment : RootFragment
    private var mSettingFragment = SettingFragment()
    private lateinit var mFavouriteFragment : FavouriteFragment
    private var mAboutFragment = AboutFragment()
    private lateinit var mEditProfileFragment: EditProfileFragment
    private var mFavouriteCount: Int = 0
    private var mIsGridView: Boolean = false
    private lateinit var mDatabaseOpenHelper: DatabaseOpenHelper
    private var mIconList = arrayOf(R.drawable.ic_home, R.drawable.ic_favourite, R.drawable.ic_setting,
        R.drawable.ic_about)
    private lateinit var mReminderAdapter: ReminderAdapter
    private lateinit var mReminderRecyclerView: RecyclerView
    private lateinit var mHeaderLayout: View
    private lateinit var mReminderList: ArrayList<Movie>
    private lateinit var mEditProfile: Button
    private lateinit var mAvatarImg: ImageView
    private lateinit var mNameProfifeTv: TextView
    private lateinit var mMailTv: TextView
    private lateinit var mDateOfBirthTv: TextView
    private lateinit var mGenderTv: TextView
    private lateinit var mShowAllReminder: Button
    private lateinit var pref: SharedPreferences
    private lateinit var imgBitmap: Bitmap
    private var mTabTitleList = arrayOf("Home", "Favourite", "Setting", "About")
    private var imgConverter = BitmapConverter()
//    private val mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDatabaseOpenHelper = DatabaseOpenHelper(this, "movie_database", null, 1)
        mFavouriteCount = mDatabaseOpenHelper.getListMovie().size
        mReminderList = mDatabaseOpenHelper.getListReminder()
        pref = PreferenceManager.getDefaultSharedPreferences(this)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.appBarMain.toolbar)
        val drawerLayout: DrawerLayout = binding.drawerLayout
        val mNavView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_favourite, R.id.nav_setting, R.id.nav_about
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        mNavView.setupWithNavController(navController)
        mHeaderLayout = mNavView.getHeaderView(0)
        setUpTab()

        //Navigation view
        mAvatarImg = mHeaderLayout.findViewById(R.id.avatar)
        mNameProfifeTv = mHeaderLayout.findViewById(R.id.name)
        mMailTv = mHeaderLayout.findViewById(R.id.mail)
        mDateOfBirthTv = mHeaderLayout.findViewById(R.id.date_of_birth)
        mGenderTv = mHeaderLayout.findViewById(R.id.gender)
        mEditProfile = mHeaderLayout.findViewById(R.id.edit)
        mShowAllReminder = mHeaderLayout.findViewById(R.id.show_all)
        mEditProfile.setOnClickListener{
            mEditProfileFragment = EditProfileFragment()
            mEditProfileFragment.setProfileListener(this)
            if (!mEditProfileFragment.isAdded) {
                supportFragmentManager.beginTransaction().apply {
                    add(R.id.drawer_layout, mEditProfileFragment)
                    addToBackStack(null)
                    commit()
                }
            }
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
    private fun setUpTab() {
        mViewPager = findViewById(R.id.view_pager)
        mTabLayout = findViewById(R.id.tab)
        mFavouriteFragment = FavouriteFragment(mDatabaseOpenHelper)
        mFavouriteFragment.setBadgeListener(this)
        mHomeFragment = HomeFragment.newInstance(1)
        mHomeFragment.setBadgeListener(this)
        mHomeFragment.setMovieListener(this)
        mHomeFragment.setDetailListener(this)
        mRootFragment = RootFragment(mHomeFragment)
        mFavouriteFragment.setFavouriteListener(this)
        val mViewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        mViewPagerAdapter.addFragment(mRootFragment, "Home")
        mViewPagerAdapter.addFragment(mFavouriteFragment, "Favourite")
        mViewPagerAdapter.addFragment(mSettingFragment, "Setting")
        mViewPagerAdapter.addFragment(mAboutFragment, "About")
        mViewPager.offscreenPageLimit = 4
        mViewPager.adapter = mViewPagerAdapter

        //Reminder

        mReminderRecyclerView = mHeaderLayout.findViewById(R.id.reminder_recycler)
        mReminderAdapter = ReminderAdapter(mReminderList, Constant.REMINDER_PROFILE, mDatabaseOpenHelper)
        mReminderAdapter.setReminderListener(this)
        mReminderRecyclerView.adapter = mReminderAdapter
        val linearLayoutManager = LinearLayoutManager(this)
        mReminderRecyclerView.layoutManager = linearLayoutManager

        createNotificationChanel()
        mViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                if (position > 1) {
                    for (i in 0 until supportFragmentManager.backStackEntryCount) {
                        supportFragmentManager.popBackStack()
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })
        mTabLayout.setupWithViewPager(mViewPager)
        val countFragment = mViewPagerAdapter.count
        for (i in 0 until  countFragment) {
            mTabLayout.getTabAt(i)!!.setIcon(mIconList[i])
            if (i == 1) {
                mTabLayout.getTabAt(1)?.setCustomView(R.layout.notification_badge)
                val badgeText = mTabLayout.getTabAt(1)!!.customView?.findViewById<TextView>(R.id.count)
                badgeText?.visibility = View.VISIBLE
                badgeText?.setText("$mFavouriteCount",TextView.BufferType.EDITABLE)
            }
        }
        setTitleFragment()
    }
    private fun setTitleFragment() {
        mViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                mTabLayout.nextFocusRightId = position
                supportActionBar?.title = mTabTitleList[position]
            }

            override fun onPageScrollStateChanged(state: Int) {
            }


        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.change_layout -> {
                mIsGridView = !mIsGridView
                if (mIsGridView) {
                    item.setIcon(R.drawable.ic_baseline_view_list_24)
                }
                else item.setIcon(R.drawable.ic_baseline_view_module_24)
                mHomeFragment.changeViewHome()
            }
            R.id.action_home -> mViewPager.currentItem = 0
            R.id.action_favourite -> mViewPager.currentItem = 1
            R.id.action_settings -> mViewPager.currentItem = 2
            R.id.action_about -> mViewPager.currentItem = 3
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onUpdateBadgeNumber(isFavourite: Boolean) {
        val tabView = mTabLayout.getTabAt(1)?.customView
        val badgeText = tabView?.findViewById<TextView>(R.id.count)
        if (isFavourite) {
            badgeText?.setText("${++mFavouriteCount}", TextView.BufferType.EDITABLE)
        }
        else badgeText?.setText("${--mFavouriteCount}", TextView.BufferType.EDITABLE)
    }

    override fun onUpdateFromFavourite(movie: Movie) {
        mHomeFragment.updateMovieList(movie, false)
        mRootFragment.setHomeFragment(mHomeFragment)

    }

    override fun onUpdateFromMovie(movie: Movie, isFavourite: Boolean) {
        mFavouriteFragment.updateFavouriteList(movie, isFavourite)
    }

    override fun onUpdateTitleMovie(movieTitle: String) {
        supportActionBar?.title = movieTitle
    }

    override fun onUpdateFromDetail(movie: Movie, isFavourite: Boolean) {
        mHomeFragment.updateMovieList(movie, isFavourite)
        mRootFragment.setHomeFragment(mHomeFragment)
        mFavouriteFragment.updateFavouriteList(movie, isFavourite)
    }

    override fun onAddReminder() {
    }

    private fun createNotificationChanel(){
        val channel = NotificationChannel(channelID, "Movie!!!", NotificationManager.IMPORTANCE_DEFAULT)
        channel.description = "Time to watch movie!!"
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    override fun onShowDetailReminder(movie: Movie) {
    }

    override fun onSaveProfile(
        name: String,
        email: String,
        dateOfBirth: String,
        gender: String,
        bitmapImage: Bitmap
    ) {
        supportActionBar?.title = mTabTitleList[0]
        val edit = pref.edit()
        edit.putString(Constant.PROFILE_NAME_KEY, name)
        edit.putString(Constant.PROFILE_EMAIL_KEY, email)
        edit.putString(Constant.PROFILE_BIRTHDAY_KEY, dateOfBirth)
        edit.putString(Constant.PROFILE_GENDER_KEY, gender)
        if (bitmapImage != null)
            edit.putString(Constant.PROFILE_AVATAR_KEY, imgConverter.encodeBase64(bitmapImage))
        this.imgBitmap = bitmapImage
        mAvatarImg.setImageBitmap(imgBitmap)
        edit.apply()
        loadDataProfile()
    }

    private fun loadDataProfile() {
        mNameProfifeTv.text = pref.getString(Constant.PROFILE_NAME_KEY, "No data")
        mMailTv.text = pref.getString(Constant.PROFILE_EMAIL_KEY, "No data")
        mDateOfBirthTv.text = pref.getString(Constant.PROFILE_BIRTHDAY_KEY, "No data")
        mGenderTv.text = pref.getString(Constant.PROFILE_GENDER_KEY, "No data")
        try {
            mAvatarImg.setImageBitmap(
                imgConverter.decodeBase64(pref.getString(Constant.PROFILE_AVATAR_KEY,"No data"))
            )
        } catch (e: Exception) {
            mAvatarImg.setImageResource(R.mipmap.avatar)
        }
    }
}
