package com.example.movie.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import android.util.*

class BitmapConverter {
    fun encodeBase64(image: Bitmap) : String? {
        val baos = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val b = baos.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    fun decodeBase64(input: String?) : Bitmap? {
        val decodeByte = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodeByte, 0, decodeByte.size)
    }
}
