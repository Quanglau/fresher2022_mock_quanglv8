package com.example.movie.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.movie.R
import com.example.movie.constant.APIConstant
import com.example.movie.constant.Constant
import com.example.movie.database.DatabaseOpenHelper
import com.example.movie.listenercallback.ReminderListener
import com.example.movie.model.Movie

class ReminderAdapter (
    private var listReminder: ArrayList<Movie>,
    private var type: Int,
    private var databaseOpenHelper: DatabaseOpenHelper
): RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private lateinit var mReminderListener: ReminderListener
    fun setReminderListener(reminderListener: ReminderListener){
        this.mReminderListener = reminderListener
    }

    fun updateData(listMovieReminder: ArrayList<Movie>) {
        this.listReminder =listMovieReminder
        notifyDataSetChanged()
    }

    fun deleteItem(index: Int) {
        val movie = listReminder[index]
        databaseOpenHelper.deleteReminder(movie.id)
        listReminder.removeAt(index)
        notifyItemRemoved(index)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ReminderViewHolder(type, mReminderListener, LayoutInflater.from(parent.context).inflate(
            R.layout.reminder_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ReminderViewHolder).bindData(listReminder[position])
    }

    override fun getItemCount(): Int {
        return  if(type == Constant.REMINDER_ALL) listReminder.size
        else if (listReminder.size > 3) 3
        else listReminder.size
    }

    class ReminderViewHolder(private val type: Int, private var mReminderListener: ReminderListener, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imgPoster = itemView.findViewById<ImageView>(R.id.r_img_poster)
        private var title = itemView.findViewById<TextView>(R.id.r_title_rate)
        private var releaseDate = itemView.findViewById<TextView>(R.id.r_release)
        fun bindData(movie: Movie) {
            val url = APIConstant.BASE_IMG_URL + movie.posterPath
            Picasso.get().load(url).into(imgPoster)
            if (type == Constant.REMINDER_ALL) imgPoster.visibility = View.VISIBLE
            else imgPoster.visibility = View.GONE
            title.text = "${movie.title} - ${movie.voteAverage}/10"
            releaseDate.text = movie.dateHourTime
            itemView.setOnClickListener {mReminderListener.onShowDetailReminder(movie)}
        }
    }

}
