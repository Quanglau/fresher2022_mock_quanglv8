package com.example.movie.adapters


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.movie.R
import com.example.movie.constant.APIConstant
import com.example.movie.model.Movie

class MovieAdapter (
    private var mListMovie: MutableList<Movie>,
    private var mScreenType: Int,
    private var mIsFavouriteList: Boolean,
    private var mViewClickListener: View.OnClickListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    fun updateData(listMovie: MutableList<Movie>) {
        this.mListMovie = listMovie
        notifyDataSetChanged()
    }

    fun settingMovieFavourite(myFavMov: ArrayList<Movie>) {
        for (i in 0 until mListMovie.size) {
            for (j in 0 until myFavMov.size) {
                if (mListMovie[i].id == myFavMov[j].id) {
                    mListMovie[i].isFavourite = true
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == 1) return ListViewHolder(
            mViewClickListener, mListMovie,
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        )
        if (viewType == 0) return GridViewHolder(
            mListMovie,
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item_grid, parent, false)
        )
        else return LoadViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.movie_item_load, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        holder.itemView.tag = position
        holder.itemView.setOnClickListener(mViewClickListener)
        if (holder is GridViewHolder) {
            holder.bindData(position)
        }
        if (holder is ListViewHolder) {
            holder.bindData(position)
        }
    }

    override fun getItemCount(): Int {
        return mListMovie.size
    }

    class GridViewHolder(private var movieList: MutableList<Movie>, itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var imgMovie = itemView.findViewById<ImageView>(R.id.image_grid)
        private var tvTitle = itemView.findViewById<TextView>(R.id.title_grid)
        fun bindData(position: Int) {
            val movie = movieList[position]
            val url = APIConstant.BASE_IMG_URL + movie.posterPath
            Picasso.get().load(url).into(imgMovie)
            tvTitle.text = movie.title
        }
    }
    class ListViewHolder(private var mViewClickListener: View.OnClickListener,
                         private var movieList: MutableList<Movie>
                         , itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var itemTitle = itemView.findViewById<TextView>(R.id.title)
        private var itemImgMovie =  itemView.findViewById<ImageView>(R.id.image)
        private var itemDate = itemView.findViewById<TextView>(R.id.date)
        private var itemRate = itemView.findViewById<TextView>(R.id.rate)
        private var itemAdult = itemView.findViewById<ImageView>(R.id.adult)
        private var itemAddFav = itemView.findViewById<ImageButton>(R.id.add_fav)
        private var itemOverview = itemView.findViewById<TextView>(R.id.overview)
        fun bindData(position: Int) {
            val movie = movieList[position]
            val url = APIConstant.BASE_IMG_URL + movie.posterPath
            Picasso.get().load(url).into(itemImgMovie)
            itemTitle.text = movie.title
            itemDate.text = movie.releaseDate
            itemRate.text = "${movie.voteAverage}/10"
            itemOverview.text = movie.overview
            if (movie.adult) {
                itemAdult.visibility = View.VISIBLE
            }
            else
                itemAdult.visibility = View.GONE
            if (movie.isFavourite) {
                itemAddFav.setImageResource(R.drawable.ic_starfish1)
            }
            else itemAddFav.setImageResource(R.drawable.ic_starfish)
            itemAddFav.tag = position
            itemAddFav.setOnClickListener(mViewClickListener)
        }
    }
    class LoadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun getItemViewType(position: Int): Int {
        if (!mIsFavouriteList && !mListMovie.isEmpty() && position == mListMovie.size - 1) return  2
        else return mScreenType
    }
}

